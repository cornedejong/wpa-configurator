package main

import (
	"flag"
	"log"
	"os"

	"solrad.nl/wpa-provisioner/routes"
)

var sddsDir string
var sdds bool
var port int

func init() {
	homedir, err := os.UserHomeDir()
	if err != nil {
		log.Fatal(err)
	}

	flag.StringVar(&sddsDir, "sdds", homedir+string(os.PathSeparator)+".sdds", "The path to the sdds config dir")
	flag.BoolVar(&sdds, "sdds-register", false, "Should we register with sdds")
	flag.IntVar(&port, "port", 7692, "The path to the sdds config dir")
}

func main() {
	flag.Parse()

	if sdds {
		go RegisterWithSdds()
	}

	/* Also advertise on UPNP */
	// go Advertise(port) // Not working.. port already in use..

	/* Listen for http request and route them */
	routes.Listen(port)
}
