package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"strconv"

	"github.com/julienschmidt/httprouter"
	"github.com/theojulienne/go-wireless"
)

func init() {
	router.DELETE("/:network", DeleteNetwork)
}

func DeleteNetwork(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	wc, err := wireless.NewClient("wlan0")
	defer wc.Close()
	if err != nil {
		log.Println("Error while connecting to wlan0: " + err.Error())
		http.Error(w, "Error while connecting to wlan0: "+err.Error(), http.StatusInternalServerError)
		return
	}

	id, err := strconv.Atoi(p.ByName("network"))
	if err != nil {
		http.Error(w, "Network id is not a number!", 400)
		return
	}

	err = wc.RemoveNetwork(id)
	if err != nil {
		http.Error(w, "Error while removing network with id "+fmt.Sprint(id), http.StatusBadRequest)
		return
	}

	json, err := json.Marshal(struct {
		Success bool
	}{Success: true})

	if err != nil {
		log.Println("Error while encoding response json: " + err.Error())
		http.Error(w, "Error while encoding response json: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	fmt.Fprint(w, string(json))
	return
}
