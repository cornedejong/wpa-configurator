package routes

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/go-playground/validator"
	"github.com/julienschmidt/httprouter"
	"github.com/theojulienne/go-wireless"
)

func init() {
	router.POST("/", CreateNetwork)
}

type WPACredentials struct {
	Ssid       string `json:"ssid" validate:"required,min=1"`
	Passphrase string `json:"passphrase" validate:"required,min=1"`
}

type IError struct {
	Field string
	Tag   string
	Value string
}

func ValidatePostData(creds WPACredentials) (bool, []*IError) {

	var Validator = validator.New()
	var errors []*IError

	err := Validator.Struct(creds)
	if err != nil {
		for _, err := range err.(validator.ValidationErrors) {
			var el IError
			el.Field = err.Field()
			el.Tag = err.Tag()
			el.Value = err.Param()
			errors = append(errors, &el)
		}

		return false, errors
	}

	return true, []*IError{}
}

func CreateNetwork(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	wc, err := wireless.NewClient("wlan0")
	defer wc.Close()
	if err != nil {
		log.Println("Error while connecting to wlan0: " + err.Error())
		http.Error(w, "Error while connecting to wlan0: "+err.Error(), http.StatusInternalServerError)
		return
	}

	var credentials WPACredentials
	err = json.NewDecoder(r.Body).Decode(&credentials)
	switch {
	case err == io.EOF:
		http.Error(w, "Missing request body!", http.StatusBadRequest)
		return
	case err != nil:
		http.Error(w, "Error while decoding input json: "+err.Error(), http.StatusBadRequest)
		return
	}

	valid, errors := ValidatePostData(credentials)
	if !valid {
		json, err := json.Marshal(errors)
		if err != nil {
			http.Error(w, "Error while validating input data: "+err.Error(), http.StatusInternalServerError)
			return
		}
		w.Header().Add("Content-Type", "application/json")
		fmt.Fprint(w, string(json))
		return
	}

	net := wireless.NewNetwork(credentials.Ssid, credentials.Passphrase)
	net, err = wc.AddNetwork(net)

	if err != nil {
		log.Println("Error while adding the new network: " + err.Error())
		http.Error(w, "Error while adding the new network: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = wc.EnableNetwork(net.ID)
	if err != nil {
		log.Println("Error while enabling the new network: " + err.Error())
		http.Error(w, "Error while enabling the new network: "+err.Error(), http.StatusInternalServerError)
		return
	}

	err = wc.SaveConfig()
	if err != nil {
		log.Println("Error while saving the new configuration: " + err.Error())
		http.Error(w, "Error while saving the new configuration: "+err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(struct {
		Success bool
		Network wireless.Network
	}{Success: true, Network: net})

	if err != nil {
		log.Println("Error while encoding response json: " + err.Error())
		http.Error(w, "Error while encoding response json: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	fmt.Fprint(w, string(json))
}
