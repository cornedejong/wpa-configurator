package routes

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"github.com/theojulienne/go-wireless"
)

var router *httprouter.Router = httprouter.New()

func init() {
	router.GET("/", ListNetworks)
}

func ListNetworks(w http.ResponseWriter, r *http.Request, p httprouter.Params) {
	wc, err := wireless.NewClient("wlan0")
	defer wc.Close()
	if err != nil {
		log.Println("Error while connecting to wlan0: " + err.Error())
		http.Error(w, "Error while connecting to wlan0: "+err.Error(), http.StatusInternalServerError)
		return
	}

	nets, err := wc.Networks()
	if err != nil {
		log.Println("Error while obtaining known networks: " + err.Error())
		http.Error(w, "Error while obtaining known networks: "+err.Error(), http.StatusInternalServerError)
		return
	}

	json, err := json.Marshal(struct {
		Networks []wireless.Network `json:"networks"`
	}{Networks: nets})

	if err != nil {
		log.Println("Error while encoding response json: " + err.Error())
		http.Error(w, "Error while encoding response json: "+err.Error(), http.StatusInternalServerError)
		return
	}

	w.Header().Add("Content-Type", "application/json")
	fmt.Fprint(w, string(json))
}

func Listen(port int) {
	log.Fatal(http.ListenAndServe(":"+fmt.Sprint(port), router))
}
