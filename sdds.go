package main

import (
	"encoding/json"
	"io/ioutil"
	"os"
	"os/signal"
)

type Service struct {
	Name           string  `json:"name"`
	ShortName      string  `json:"shortName"`
	Version        float32 `json:"version"`
	Type           string  `json:"type"`
	Port           int     `json:"port"`
	ConnectionType string  `json:"connectionType"`
	Protocol       string  `json:"protocol"`
}

func RegisterWithSdds() {
	/* TODO: Register this service with sdds on device */
	file, err := json.Marshal(Service{
		Name:           "WiFi Configurator",
		ShortName:      "wpa-wifi-prov",
		Version:        1.0,
		Type:           "configurator",
		Port:           port,
		ConnectionType: "tcp",
		Protocol:       "http",
	})

	if err != nil {
		panic(err)
	}

	serviceDirectory := sddsDir + string(os.PathSeparator) + "services"

	err = ioutil.WriteFile(serviceDirectory+string(os.PathSeparator)+"wpa-wifi-prov.json", file, 0644)
	if err != nil {
		panic(err)
	}
	/* TODO: un register this service when exiting (defer) */
	defer func() {
		err := os.Remove(serviceDirectory + string(os.PathSeparator) + "wpa-wifi-prov.json")
		if err != nil {
			panic(err)
		}
	}()

	/* Lets set always send the byebye event when exiting the application */
	/* We capture the interrupt signal and before exiting for real we send the event */
	c := make(chan os.Signal, 1)
	/* Lets get notified when an interrupt happens */
	signal.Notify(c, os.Interrupt)

	for _ = range c {
		err := os.Remove(serviceDirectory + string(os.PathSeparator) + "wpa-wifi-prov.json")
		if err != nil {
			panic(err)
		}
		/* And now honour the interrupt */
		os.Exit(0)
	}
}
