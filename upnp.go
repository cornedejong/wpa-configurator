package main

import (
	"fmt"
	"time"

	"github.com/koron/go-ssdp"
)

/* Should run as a goroutine */
func Advertise(port int) {
	ad, err := ssdp.Advertise(
		"urn:solrad:device:webbox",                  // send as "ST"
		"uuid:d4c78e9b-ea4e-448a-a8b0-39eb1b33af91", // send as "USN"
		"http://192.168.179.90:"+fmt.Sprint(port),   // send as "LOCATION"
		"webbox", // send as "SERVER"
		1800)     // send as "maxAge" in "CACHE-CONTROL"
	if err != nil {
		panic(err)
	}

	quit := make(chan bool)
	aliveTick := time.Tick(300 * time.Second)

	for {
		select {
		case <-aliveTick:
			ad.Alive()
		case <-quit:
			break
		}
	}

	// send/multicast "byebye" message.
	ad.Bye()
	// teminate Advertiser.
	ad.Close()
}
